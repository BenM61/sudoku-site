import Cell from "./Cell";

const Box = (props) => {
  const splitToRows = (box) => {
    let res = [];
    const rowDim = Math.sqrt(box.length);

    for (let i = 0; i < rowDim; i++) {
      res.push([]);
      for (let j = i * rowDim; j < (i + 1) * rowDim; j++) {
        res[i].push(box[j]);
      }
    }
    return res;
  };

  const { data, gridBoxIndex, inputGrid, setInputGrid } = props;
  const rows = splitToRows(data);

  return (
    <div className="box">
      {rows.map((row, brOuter) => (
        <div className="box-inner-row" key={brOuter}>
          {row.map((digit, brInner) => (
            <Cell
              key={brInner}
              data={data}
              digit={digit}
              brOuter={brOuter}
              brInner={brInner}
              gridBoxIndex={gridBoxIndex}
              inputGrid={inputGrid}
              setInputGrid={setInputGrid}
            />
          ))}
        </div>
      ))}
    </div>
  );
};

export default Box;
