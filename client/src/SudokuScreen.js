import { useEffect, useState } from "react";
import Axios from "axios";

import Grid from "./Grid";
import SideBar from "./SideBar";

const SudokuScreen = () => {
  const [size, setSize] = useState(9);
  const [inputSudoku, setInputSudoku] = useState([]);
  const [solveDetails, setSolveDetails] = useState([]);

  useEffect(() => {
    const getSolverDetailsHelper = async () => {
      const res = await getSolveDetails(inputSudoku);
      setSolveDetails(res);
    };
    if (!isSolveValid(inputSudoku, solveDetails.solution)) {
      getSolverDetailsHelper();
    }
  }, [inputSudoku]);

  useEffect(() => {
    const zeroMat = [];
    for (let i = 0; i < size; i++) {
      zeroMat.push([]);
      for (let j = 0; j < size; j++) {
        zeroMat[i].push(0);
      }
    }
    setInputSudoku(zeroMat);
  }, [size]);

  const getSolveDetails = async (grid) => {
    const res = await Axios.get(`http://localhost:7000/solution`, {
      params: { grid },
    });
    return res.data;
  };

  // naiive check- valid iff the solution contains the input
  const isSolveValid = (input, solution) => {
    if (!solution || solution.length !== input.length) {
      return false;
    }
    const n = solution.length;
    for (let i = 0; i < n; i++) {
      for (let j = 0; j < n; j++) {
        if (input[i][j] > 0 && solution[i][j] !== input[i][j]) {
          return false;
        }
      }
    }
    return true;
  };

  const hint = async () => {
    if (!isSolveValid(inputSudoku, solveDetails.solvedSudoko)) {
      setSolveDetails(await getSolveDetails(inputSudoku));
    }

    if (!solveDetails.solvable) {
      console.log("nope");
      return;
    }

    //take a step at random and fill it in
    const stepIndex = Math.floor(Math.random() * solveDetails.steps.length);
    const addedStep = solveDetails.steps[stepIndex];
    inputSudoku[addedStep.x][addedStep.y] = addedStep.value;
    setInputSudoku([...inputSudoku]);
    solveDetails.steps.splice(stepIndex, 1);
    console.log("got hint");
  };

  const solve = async () => {
    if (!isSolveValid(inputSudoku, solveDetails.solution)) {
      setSolveDetails(await getSolveDetails(inputSudoku));
    }
    console.log(solveDetails);
    setInputSudoku(solveDetails.solution);

    if (!solveDetails.solvable) {
      console.log("nope");
      return;
    }
  };

  return (
    <div className="sudoku-screen">
      <Grid
        inputGrid={inputSudoku}
        setInputGrid={setInputSudoku}
        solvable={solveDetails.solvable}
      />
      <br></br>
      <SideBar
        size={size}
        setSize={setSize}
        solve={solve}
        hint={hint}
      ></SideBar>
    </div>
  );
};

export default SudokuScreen;
