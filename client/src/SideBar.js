const SideBar = (props) => {
  const changeSize = (e) => {
    setSize(parseFloat(e.target.value));
  };

  const { size, setSize, solve, hint } = props;

  return (
    <div>
      <div className="buttons">
        <button onClick={hint}>Hint</button>
        <button onClick={solve}>solution</button>
      </div>
      <div className="size-changer">
        <label>Select the appropriate size </label> <br />
        <input
          type="radio"
          value="4"
          name="gridSize"
          checked={size === 4}
          onChange={changeSize}
        />{" "}
        2 square / 4 across
        <input
          type="radio"
          value="9"
          name="gridSize"
          checked={size === 9}
          onChange={changeSize}
        />{" "}
        3 square / 9 across
        <input
          type="radio"
          value="16"
          name="gridSize"
          checked={size === 16}
          onChange={changeSize}
        />{" "}
        4 square / 16 across
        <input
          type="radio"
          value="25"
          name="gridSize"
          checked={size === 25}
          onChange={changeSize}
        />{" "}
        5 square / 25 across
      </div>
    </div>
  );
};
export default SideBar;
