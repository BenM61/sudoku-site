import { useEffect, useState } from "react"

import Box from "./Box"

const Grid = (props) => {
  const rowColumn2blockPositionHelper = (i, j, num) => {
    let boxDim = Math.sqrt(num)
    let block = (Math.floor(i / boxDim)) * boxDim + Math.floor(j / boxDim)
    let position = (i % boxDim) * boxDim + j % boxDim
    return [block, position]
  }

  const rowColumn2blockPosition = (grid, size) => {
    // initialize grid
    let res = []
    for (let i = 0; i < size; i++) {
      res.push([])
      for (let j = 0; j < size; j++) {
        res[i].push(0)
      }
    }

    for (let i = 0; i < size; i++) {
      for (let j = 0; j < size; j++) {
        let retva = rowColumn2blockPositionHelper(i, j, size)
        let block = retva[0]
        let position = retva[1]
        res[block][position] = grid[i][j]
      }
    }
    return res
  }

  const getBoxRows = (blockPositionGrid) => {
    let res = []
    const boxDim = Math.sqrt(blockPositionGrid.length)

    for (let i = 0; i < boxDim; i++) {
      res.push([])
      for (let j = i * boxDim; j < (i + 1) * boxDim; j++) {
        res[i].push(blockPositionGrid[j])
      }
    }
    return res
  }

  const isWrong = () => !(solvable === undefined || solvable)

  const [boxRows, setBoxRows] = useState([])
  const { inputGrid, setInputGrid, solvable } = props
  const gridRowLen = inputGrid.length
  const boxRowAmount = Math.sqrt(gridRowLen)

  const solveableStyle = { border: "5px solid black" }
  const unsolveableStyle = { border: "5px solid red" }
  const style = !isWrong() ? solveableStyle : unsolveableStyle

  useEffect(() => {
    setBoxRows(getBoxRows(rowColumn2blockPosition(inputGrid, gridRowLen)))
  }, [inputGrid])



  return (
    <div className="Grid">
      {isWrong() && <div>unsolvable</div>}
      {boxRows.map((boxRow, grOuter) =>
      (<div className="boxes-row" key={grOuter}>
        {boxRow.map((box, grInner) => (
          <div className="box-div" key={grInner} style={style}>
            <Box data={box} gridBoxIndex={grOuter * boxRowAmount + grInner}
              inputGrid={inputGrid} setInputGrid={setInputGrid} />
          </div>))}
      </div>))}
    </div>
  );
}

export default Grid;