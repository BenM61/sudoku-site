import SudokuScreen from "./SudokuScreen";

function App() {
  return (
    <div className="App">
      <div className="app-content">
        <div className="app-header">
          <h1>Please enter your current puzzle, then click on one of the options</h1>
        </div>
        <SudokuScreen />
      </div>
    </div>
  );
}

export default App;
