const Cell = (props) => {
  const {
    data,
    digit,
    brOuter,
    brInner,
    gridBoxIndex,
    inputGrid,
    setInputGrid,
  } = props;

  const blockPosition2rowColumn = (block, position, num) => {
    let boxDim = Math.sqrt(num);
    let i = Math.floor(block / boxDim) * boxDim + Math.floor(position / boxDim);
    let j = (block % boxDim) * boxDim + (position % boxDim);
    return [i, j];
  };

  const handleChange = (e, brOuter, brInner) => {
    const len = Math.sqrt(data.length);
    const boxIndex = len * brOuter + brInner;
    const [i, j] = blockPosition2rowColumn(gridBoxIndex, boxIndex, len * len);

    // save the first input digit if exists
    const _isNonzeroDigit = (value) => /^[1-9]$/.test(value);
    const userInput = e.target.value;
    let value = 0;
    switch (userInput.length) {
      case 0:
        break;
      case 1:
        value = _isNonzeroDigit(userInput) ? parseFloat(userInput) : 0;
        break;
      default:
        value = parseFloat(userInput.charAt(0));
        break;
    }
    inputGrid[i][j] = value;
    setInputGrid([...inputGrid]);
  };

  return (
    <div className="cell" key={brInner}>
      <input
        value={digit === 0 ? "" : digit}
        onChange={(e) => handleChange(e, brOuter, brInner)}
      ></input>
    </div>
  );
};

export default Cell;
