const express = require("express")
const cors = require("cors")

const solver = require("./solver")
const { getSolutionAndStatus, getSteps } = solver

const app = express()
app.use(express.json())
app.use(cors({
  origin: "http://localhost:3000"
}))

const PORT = 7000

app.get("/solution", (req, res) => {
  let result
  try {
    let grid = req.query.grid
    grid = grid.map(row => JSON.parse(row))
    const [solvable, solution] = getSolutionAndStatus(grid)
    const steps = getSteps(grid, solution)
    result = JSON.stringify({ solvable, solution, steps })
  }
  catch (err) {
    result = `grid error: ${err}`
  }
  res.send(result)
})

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`)
})