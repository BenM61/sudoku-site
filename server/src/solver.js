const _getNextBlank = (n, grid, prevX = 0, prevY = 0) => {
  let startRow = prevX
  if (prevY === n - 1) {
    startRow = prevX + 1
  }

  for (let i = startRow; i < n; i++) {
    for (let j = 0; j < n; j++) {
      if (grid[i][j] === 0) {
        return [i, j]
      }
    }
  }
  return [-1, -1]
}

const __isRowSafe = (n, grid, x) => {
  const values = new Set()
  for (let j = 0; j < n; j++) {
    const curr = grid[x][j]
    if (curr !== 0 && values.has(curr)) {
      return false
    }
    values.add(curr)
  }
  return true
}

const __isColumnSafe = (n, grid, y) => {
  const values = new Set()
  for (let i = 0; i < n; i++) {
    const curr = grid[i][y]
    if (curr !== 0 && values.has(curr)) {
      return false
    }
    values.add(curr)
  }
  return true
}

const __isBoxSafe = (n, grid, x, y) => {
  const values = new Set()
  const boxDim = Math.sqrt(n)
  boxX = Math.floor(x / boxDim)
  boxY = Math.floor(y / boxDim)

  for (let i = boxX * boxDim; i < (boxX + 1) * boxDim; i++) {
    for (let j = boxY * boxDim; j < (boxY + 1) * boxDim; j++) {
      const curr = grid[i][j]
      if (curr !== 0 && values.has(curr)) {
        return false
      }
      values.add(curr)
    }
  }
  return true
}

const _isSafe = (n, grid, x, y) => {
  const isRowSafe = __isRowSafe(n, grid, x)
  const isColumnSafe = __isColumnSafe(n, grid, y)
  const isBoxSafe = __isBoxSafe(n, grid, x, y)
  return isRowSafe && isColumnSafe && isBoxSafe
}

const _solve = (grid) => {
  // deep copy the grid
  grid = JSON.parse(JSON.stringify(grid))

  const n = grid.length
  const [x, y] = _getNextBlank(n, grid)

  let solvable = false
  if (_solveRec(n, grid, x, y)) {
    solvable = true
  }

  return [solvable, grid]
}

const _solveRec = (n, grid, x, y) => {
  // reached end of grid
  if (x === -1) {
    return true
  }

  // try to fill the rest
  for (let i = 1; i <= n; i++) {
    grid[x][y] = i
    if (_isSafe(n, grid, x, y)) {
      const [newX, newY] = _getNextBlank(n, grid, x, y)
      if (_solveRec(n, grid, newX, newY)) {
        return true
      }
    }
  }
  // undo in case of failure
  grid[x][y] = 0
  return false
}

const _isInitialGridValid = (grid) => {
  const n = grid.length

  for (let i = 0; i < n; i++) {
    const isRowSafe = __isRowSafe(n, grid, i)
    const isColumnSafe = __isColumnSafe(n, grid, i)
    if (!(isRowSafe && isColumnSafe)) {
      return false
    }
  }
  const boxSize = Math.sqrt(n)
  for (let x = 0; x < n; x += boxSize) {
    for (let y = 0; y < n; y += boxSize) {
      if (!__isBoxSafe(n, grid, x, y)) {
        return false
      }
    }
  }
  return true
}

const getSolutionAndStatus = (grid) => {
  if (!_isInitialGridValid(grid)) {
    return [false, grid]
  }
  return _solve(grid)
}

const getSteps = (initialGrid, solution) => {
  const n = initialGrid.length
  const res = []
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < n; j++) {
      if (initialGrid[i][j] === 0) {
        res.push({ x: i, y: j, value: solution[i][j] })
      }
    }
  }
  return res
}

module.exports = { getSolutionAndStatus, getSteps }